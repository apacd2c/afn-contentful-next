const webpack = require('webpack');

module.exports = {
  target: 'serverless',
  webpack: (config, { dev }) => {
    config.module.rules.push({
      test: /\.svg$/,
      issuer: {
        test: /\.(js|ts)x?$/,
      },
      use: ['@svgr/webpack'],
    });

    const jqueryProvidePlugin = new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
    });

    config.plugins.push(jqueryProvidePlugin);
    return config;
  },
  env: {
    CONTENTFUL_BRANCH: process.env.CONTENTFUL_BRANCH,
    CONTENTFUL_SPACE_ID: process.env.CONTENTFUL_SPACE_ID,
    CONTENTFUL_IS_PREVIEW: process.env.CONTENTFUL_IS_PREVIEW === 'true',
    CONTENTFUL_DELIVERY_TOKEN: process.env.CONTENTFUL_DELIVERY_TOKEN,
    CONTENTFUL_PREVIEW_TOKEN: process.env.CONTENTFUL_PREVIEW_TOKEN,
  },
};

Object.assign(module.exports, require('next-images')());
