import React from 'react';
import { BrowserRouter } from 'react-router-dom';
const isServer = typeof window === 'undefined';
import { ApolloProvider } from 'react-apollo';
import { client } from '../config/apollo-client';
import PropTypes from 'prop-types';

export default (App) => {
  /**
   * AppWithReactRouter
   */
  class AppWithReactRouter extends React.Component {
    /**
     * @param {*} appContext
     */
    static async getInitialProps(appContext) {
      if (!appContext.ctx.req) {
        appContext.ctx.req = { originalUrl: '/' };
      }
      const {
        ctx: {
          req: { originalUrl, locals = {} },
        },
      } = appContext;
      return {
        originalUrl,
        context: locals.context || {},
      };
    }

    /**
     * @return {component}
     */
    render() {
      if (isServer) {
        const { StaticRouter } = require('react-router');
        return (
          <StaticRouter location={this.props.originalUrl} context={this.props.context}>
            <App {...this.props} />
          </StaticRouter>
        );
      }

      return (
        <ApolloProvider client={client}>
          <BrowserRouter>
            <App {...this.props} />
          </BrowserRouter>
        </ApolloProvider>
      );
    }
  }
  AppWithReactRouter.propTypes = {
    originalUrl: PropTypes.string.isRequired,
    context: PropTypes.object.isRequired,
  };
  return AppWithReactRouter;
};
