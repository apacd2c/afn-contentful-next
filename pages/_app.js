import React from 'react';
import { ApolloProvider } from '@apollo/react-hooks';
import PropTypes from 'prop-types';

import withData from '../config/apollo-client';

import '../resources/scss/main.scss';
import '../resources/scss/vendor.scss';
import 'react-owl-carousel2/lib/styles.css';
import 'react-owl-carousel2/src/owl.theme.default.css';

/**
 * Generic wrapper for NextJS App
 *
 * @param {*} Component
 * @param {*} pageProps
 * @param {*} apollo
 * @return {app}
 */
function MyApp({ Component, pageProps, apollo }) {
  return (
    <>
      <ApolloProvider client={apollo}>
        <Component {...pageProps} />
      </ApolloProvider>
    </>
  );
}
MyApp.propTypes = {
  Component: PropTypes.func.isRequired,
  pageProps: PropTypes.object,
  apollo: PropTypes.object,
};

export default withData(MyApp);
